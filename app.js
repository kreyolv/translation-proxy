const express = require('express');
const https = require('https');
const fs = require('fs');
const app = express();
const AdmZip = require('adm-zip');
const request = require('request');
const { LokaliseApi } = require('@lokalise/node-api');
const JSZip = require('jszip');

const port = 3000;

const lokaliseApi = new LokaliseApi({ apiKey: 'ca0fb687fa428b32630d45bd9e728c2aa9ab888c' });
lokaliseApi.files.download('515787315e4fca89e1c508.99656678', { format: 'json', original_filenames: true })
    .then(result => {
        let bufs = [];
        let buf;
        request.get(result.bundle_url)
            .on('end', () => {
                buf = Buffer.concat(bufs);
                JSZip.loadAsync(buf)
                    .then(zip => {
                        zip
                            .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                            .pipe(fs.createWriteStream('out.zip'))
                            .on('finish', function () {
                                const zipFile = new AdmZip('out.zip');
                                zipFile.extractAllTo('.', true);
                                console.log('out.zip written.');
                            });
                    }).catch((error) => {
                    console.log(error);
                });
            })
            .on('error', (error) => {
                console.log(error);
            })
            .on('data', (d) => {
                bufs.push(d);
            })
    });


app.get('/', (req, res) => {
    let locale = req.query.locale;
    if (!locale) {
        locale = 'en';
    }
    let result = {};
    fs.readdirSync(locale).forEach(file => {
        let jsonString = fs.readFileSync(`./${locale}/${file}`, 'utf8');
        result = { ...result, ...JSON.parse(jsonString) };
    });
    setTimeout(() => res.json(result), 0);
});

app.listen(port, () => {
    console.log(`Translation API Proxy listening at http://localhost:${port}`)
});
